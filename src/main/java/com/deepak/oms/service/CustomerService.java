package com.deepak.oms.service;

import java.util.List;

import com.deepak.oms.model.Customer;

public interface CustomerService {
    public void addCustomer(Customer c);
    public void updateCustomer(Customer c);
    public List<Customer> listCustomers();
    public Customer getCustomerById(int id);
    public void removeCustomer(int id);
    public List<Customer> showCusName();
}


package com.deepak.oms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import com.deepak.oms.model.Customer;
import com.deepak.oms.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping(value="/customers")
    public List<Customer> listCustomers() {
        return customerService.listCustomers();
    }

    @PostMapping(value="/add")
    public void addCustomer(@ModelAttribute("customer") Customer c) {
        if(c.getCustomerId()== 0){
            // New product, add it.
            this.customerService.addCustomer(c);
        }else{
            // existing product, call update
            this.customerService.updateCustomer(c);
        }
    }

    @DeleteMapping(value = "/remove/{customerId}")
    public void removeCustomer(@PathVariable("customerId") int id) {
         customerService.removeCustomer(id);
    }

    @PutMapping(value="/edit")
    public void editCustomerById(@RequestBody Customer c) {
        customerService.updateCustomer(c);
    }


}


package com.deepak.oms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.*;

import com.deepak.oms.model.Orders;
import com.deepak.oms.service.OrdersService;

@RestController
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    @GetMapping(value="/orders")
    public List<Orders> listOrders() {
        return ordersService.listOrders();

    }

    @PostMapping(value="/add")
    public void addOrders(@ModelAttribute("order") Orders o) {
        if(o.getOrderID()== 0){
            // New orders, add it.
            ordersService.addOrders(o);
        }else{
            // existing product, call update
            ordersService.updateOrders(o);
        }
    }

    @DeleteMapping(value = "/remove/{orderId}")
    public void removeOrders(@PathVariable("orderId") int id) {
        ordersService.removeOrders(id);
    }

    @PutMapping(value="/edit")
    public void editOrdersById(@RequestBody Orders order) {
        ordersService.updateOrders(order);
    }




}

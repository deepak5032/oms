package com.deepak.oms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.deepak.oms.model.OrderDetails;
import com.deepak.oms.service.OrderDetailsService;

import java.util.List;

@RestController
@RequestMapping("/orderDetails")
public class OrderDetailsController {
    @Autowired
    private OrderDetailsService orderDetailsService;

    @GetMapping(value="/allOrderDetails")
    public List<OrderDetails> listOrderDetails() {
        return orderDetailsService.orderDetailsList();
    }


    @PostMapping(value="/add")
    public void addOrderDetails(@ModelAttribute("orderDetails") OrderDetails od) {
        if(od.getOrderDetailsNo()== 0){
            // New orders, add it.
            orderDetailsService.addOrderDetails(od);
        }else{
            // existing product, call update
            orderDetailsService.updateOrderDetails(od);
        }
    }

    @DeleteMapping(value = "/remove/{orderDetailsNo}")
    public void removeOrders(@PathVariable("orderDetailsNo") int id) {
        orderDetailsService.removeOrderDetails(id);
    }

    @PutMapping(value="/orderDetails/edit")
    public void editOrderDetailsById(@RequestBody OrderDetails od) {
        orderDetailsService.updateOrderDetails(od);
    }


}


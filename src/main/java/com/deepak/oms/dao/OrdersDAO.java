package com.deepak.oms.dao;

import java.util.List;

import com.deepak.oms.model.Customer;
import com.deepak.oms.model.Orders;

public interface OrdersDAO {
    public void addOrders(Orders o);
    public void updateOrders(Orders o);
    public List<Orders> listOrders();
    public List<Orders> getCustomername();
    public Orders getOrdersById(int id);
    public void removeOrders(int id);
    public List<Customer> listCustomer();
}
